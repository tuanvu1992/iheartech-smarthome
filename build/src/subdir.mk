################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../src/debug.c 

CC_SRCS += \
../src/Application.cc \
../src/Devices.cc \
../src/LocalServer.cc \
../src/RemoteServer.cc \
../src/SimpleGPIO.cc 

OBJS += \
./src/Application.o \
./src/Devices.o \
./src/LocalServer.o \
./src/RemoteServer.o \
./src/SimpleGPIO.o \
./src/debug.o 

C_DEPS += \
./src/debug.d 

CC_DEPS += \
./src/Application.d \
./src/Devices.d \
./src/LocalServer.d \
./src/RemoteServer.d \
./src/SimpleGPIO.d 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.cc
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -I../include/ -O0 -g3 -Wall -c -fmessage-length=0 -std=c++11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/%.o: ../src/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C Compiler'
	gcc -I../include -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


