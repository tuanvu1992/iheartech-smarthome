/*
 * LocalServer.cpp
 *
 *  Created on: Oct 5, 2015
 *      Author: messier
 */

#include "LocalServer.h"

namespace iHearTech {

LocalServer::LocalServer(LocalMode mode, const char *addr) {
	this->listenfd = 0;
	this->servfd = 0;
	this->listenfd = socket(AF_INET, SOCK_STREAM, 0);
	DEBUG_INFO("Socket retrieve success");
	memset(&this->serv_addr, '0', sizeof(this->serv_addr));
	memset(this->sendBuff, '0', sizeof(this->sendBuff));
	serv_addr.sin_family = AF_INET;
	serv_addr.sin_port = htons(5000);
	if(mode == SERVER){
		serv_addr.sin_addr.s_addr = htonl(INADDR_ANY);
		bind(listenfd, (struct sockaddr*)&serv_addr,sizeof(serv_addr));
		if(listen(listenfd, 10) == -1){
			DEBUG_ERROR("Failed to listen");
			return;
		}
	}else if (mode == CLIENT){
		serv_addr.sin_addr.s_addr = inet_addr(addr);
		if((this->servfd = socket(AF_INET, SOCK_STREAM, 0))< 0){
			DEBUG_ERROR("Could not create socket");
			return;
		}
		if(connect(this->servfd, (struct sockaddr *)&serv_addr, sizeof(serv_addr))<0)
		{
			DEBUG_ERROR("Connect Failed");
			return;
		}
	}
}

LocalServer::~LocalServer() {
	// TODO Auto-generated destructor stub
}

void LocalServer::send(int connfd, char *msg, int size) {
	DEBUG_INFO("Sending message, connfd = %d", connfd);
	int n;
	n = write(connfd, msg, size);
	if(n < 0) DEBUG_ERROR("Failed to send message, connfd = %d",connfd);
	else DEBUG_INFO("Message sent, connfd = %d",connfd);
}

int LocalServer::acceptConnection(){
	DEBUG_INFO("Waiting for connection...");
	return accept(this->listenfd, (struct sockaddr*)NULL ,NULL); // accept awaiting request
}

int LocalServer::receive(int connfd, char *msg, int size) {
	int n;
	DEBUG_INFO("Waiting for client data...");
	memset(msg, 0, size);
	n = read(connfd, msg, size);
	if(n > 0) msg[n] = 0;
	else if(n < 0) DEBUG_ERROR("Read failed!");
	return n;
}

void LocalServer::closeConnection(int connfd){
	DEBUG_INFO("Close connection %d", connfd);
	close(connfd);
}

} /* namespace iHearTech */
