/*
 * Devices.cpp
 *
 *  Created on: Sep 22, 2015
 *      Author: messier
 */

#include "Devices.h"

namespace iHearTech {

std::vector<IOMap_t> Devices::ioMap = {
		{(char*)"DENKHACH",       115, OFF},
		{(char*)"QUATKHACH",      66,  OFF},
		{(char*)"TIVI",           20,  OFF},
		{(char*)"CUACHINH",       61,  OFF},
		{(char*)"DENTHANG",       112, OFF},
		{(char*)"DENBEP",         49,  OFF},
		{(char*)"QUATBEP",        14,  OFF},
		{(char*)"DENTAMC",        3,   OFF},
		{(char*)"NNONGTAMC",      15,  OFF},
		{(char*)"DENNGU1",        5,   OFF},
		{(char*)"DENNGU2",        46,  OFF},
		{(char*)"DENNGU3",        26,  OFF},
		{(char*)"QUATNGU1",       68,  OFF},
		{(char*)"QUATNGU2",       65,  OFF},
		{(char*)"QUATNGU3",       30,  OFF},
		{(char*)"DENDOCSACH",     4,   OFF},
		{(char*)"QUATDOCSACH",    45,  OFF},
		{(char*)"DENTAM1",        69,  OFF},
		{(char*)"DENTAM2",        2,   OFF},
		{(char*)"DENTAM3",        60,  OFF},
		{(char*)"MAYNNONG1",      67,  OFF},
		{(char*)"MAYNNONG2",      47,  OFF},
		{(char*)"MAYNNONG3",      44,  OFF}
};

std::vector<IOMap_t> Devices::ioMap_cache = Devices::ioMap;

Devices::Devices() {

}

Devices::~Devices() {
	// TODO Auto-generated destructor stub
}

void Devices::init(){
	DEBUG_INFO("Initializing all devices...");
	std::vector<IOMap_t>::iterator it;
	for(it = ioMap.begin(); it != ioMap.end(); ++it){
		gpio_export(it->ioNum);
		gpio_set_dir(it->ioNum, OUTPUT_PIN);
		if(it->state == ON){
			gpio_set_value(it->ioNum, HIGH);
		}else gpio_set_value(it->ioNum, LOW);
		DEBUG_INFO("Exported GPIO%d", it->ioNum);
	}
}

void Devices::setStateRemote(string strPhp){
	std::vector<IOMap_t>::iterator it;
	std::string value_str;
	int value;
	size_t found;
	for(it = ioMap_cache.begin(); it != ioMap_cache.end(); ++it){
		found = strPhp.find(it->name,0);
		if(found != std::string::npos){
			value_str = strPhp.substr(found + strlen(it->name) + 2, 2);
			value = atoi(value_str.c_str());
			//DEBUG_INFO("Remote state %s is %d", it->name, value);
			if(value != 0){					// remote state is 'ON'
				if(it->state == OFF){		// If cache state is 'OFF' -> switch it to 'ON'
					it->state = ON;
					Devices::ioMap[it - Devices::ioMap_cache.begin()].state = ON;
					gpio_set_value(it->ioNum, HIGH);
					DEBUG_INFO("%s is switched to ON", it->name);
				}
			}else{							// remote state is 'OFF'
				if(it->state == ON){		// If local state is 'ON' -> switch it to 'OFF'
					it->state = OFF;
					Devices::ioMap[it - Devices::ioMap_cache.begin()].state = OFF;
					gpio_set_value(it->ioNum, LOW);
					DEBUG_INFO("%s is switched to OFF", it->name);
				}
			}
		}
	}
}

void Devices::setStateLocal(std::string strCmd) {
	int value;
	std::string value_str;
	std::vector<IOMap_t>::iterator it;
	std::size_t found;
	for(it = ioMap.begin(); it != ioMap.end(); ++it){
		found = strCmd.find(it->name, 0);
		if(found != std::string::npos){
			value_str = strCmd.substr(found + strlen(it->name) + 2, 1);
			value = atoi(value_str.c_str());
			//DEBUG_INFO("Remote state %s is %d", it->name, value);
			if(value != 0){					// remote state is 'ON'
				if(it->state == OFF){		// If cache state is 'OFF' -> switch it to 'ON'
					it->state = ON;
					gpio_set_value(it->ioNum, HIGH);
					DEBUG_INFO("%s is switched to ON", it->name);
				}
			}else{							// remote state is 'OFF'
				if(it->state == ON){		// If local state is 'ON' -> switch it to 'OFF'
					it->state = OFF;
					gpio_set_value(it->ioNum, LOW);
					DEBUG_INFO("%s is switched to OFF", it->name);
				}
			}
		}
	}
}

} /* namespace iHearTech */


