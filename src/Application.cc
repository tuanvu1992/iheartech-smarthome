/*
 * Application.cc
 *
 *  Created on: Sep 22, 2015
 *      Author: messier
 */
#include "Devices.h"
#include "RemoteServer.h"
#include "LocalServer.h"
#include "debug.h"
#include <pthread.h>
#include <unistd.h>
#include <ctime>
#include <sstream>
#include <iostream>

using namespace iHearTech;
using namespace std;

#define LOCAL_MSG_SIZE		1024
#define MAX_CLIENT_NUMBER	10

int num_client = 0;
int start = 0;

void *clientThread(void *t){
	long connfd = (long)t;
	DEBUG_INFO("Commencing connection %d", connfd);
	int n;
	char *msg = (char *)calloc(LOCAL_MSG_SIZE, sizeof(char));
	if(msg == NULL){
		DEBUG_FATAL("Cannot allocate memory, connection %d", connfd);
		pthread_exit(NULL);
	}
	while((n = LocalServer::receive(connfd, msg, LOCAL_MSG_SIZE)) > 0){
		DEBUG_INFO("Received new data from local server");
		if(start == 0){
			if(strstr(msg, "INFO") != NULL){
				start = 1;
			}else system("aplay ../begin.wav");
			break;
		}
		Devices::setStateLocal(std::string(msg));
		if(strstr(msg, "INFO") != NULL) system("aplay ../info.wav");
	}
	LocalServer::closeConnection(connfd);
	num_client--;
	pthread_exit(NULL);
}

void *localSrvThread(void *t){
	DEBUG_INFO("Smarthome server thread is running...");
	LocalServer *local = new LocalServer(SERVER, NULL);
	int connfd;
	pthread_t thread_client;
	while(1){
		connfd = local->acceptConnection();
		// Pose new thread for new connection
		if(num_client < MAX_CLIENT_NUMBER){
			if(pthread_create(&thread_client, NULL, clientThread,(void *)connfd)){
				DEBUG_ERROR("Cannot create thread, num_client=%d, connfd=%d", num_client, connfd);
				delete local;
				pthread_exit(NULL);
			}else{
				num_client++;
				DEBUG_INFO("Created client thread id=%d, number of client: %d", thread_client, num_client);
			}
		}else{
			DEBUG_ERROR("Maximum client exceeded. Cannot connect new client!");
		}
	}
	delete local;
	pthread_exit(NULL);
}

int main(int argc, char *argv[]){
	DEBUG_INIT();
	pthread_t thread_localSrv;
	int rc;
	Devices::init();
	rc = pthread_create(&thread_localSrv, NULL, localSrvThread, NULL);
	if(rc){
		DEBUG_FATAL("Cannot create localSrv thread. Return rc = %d",rc);
		exit(-1);
	}
	DEBUG_INFO("Initialization completed");
	system("aplay ../begin.wav");
	while(1){
		//Devices::setStateRemote(RemoteServer::receiveData());
		usleep(200000);
	}
	pthread_exit(NULL);
	return 0;
}

