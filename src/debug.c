/*
 * debug.c
 *
 *  Created on: Jul 3, 2015
 *      Author: messier
 */
#include <stdio.h>
#include <stdbool.h>
#include <stdarg.h>
#include <stdlib.h>
#include <string.h>
#include "config.h"
#include "time.h"

// Comment out to disable debug when compile code
#define CONFIG_FILE_RELATIVE_PATH	"/config/logtrace.conf"
#define LOG_BUFFER_SIZE		1024
#define TIME_BUFFER_SIZE	128
#define KNRM  "\x1B[0m"
#define KRED  "\x1B[31m"
#define KGRN  "\x1B[32m"

// Runtime option
#ifdef __DEBUG__
bool DEBUG_ENABLE = false;

#endif

void debug_init(){
#ifdef __DEBUG__
	FILE *log_file;
	DEBUG_ENABLE = true;
	// Discard old log file
	if(LOG_FILE_PATH != NULL){
		log_file = fopen(LOG_FILE_PATH, "w");
		if(log_file == NULL) DEBUG_ENABLE = false;
		fclose(log_file);
	}else{
		DEBUG_ENABLE = false;
	}
#endif
}

void debug_log(const char *severity, const char *file, const char *func, const int line,...){
#ifdef __DEBUG__
	if(DEBUG_ENABLE){
		FILE *log_file = fopen(LOG_FILE_PATH, "a");
		va_list args;
		va_start (args, line);
		char *msg = va_arg(args,char*);
		char *buffer = (char *)calloc(LOG_BUFFER_SIZE, sizeof(char));
		// Log file format
		sprintf(buffer, "[%s %s] %s %s::%s::%i	%s\n", date, time, severity,
				file, func, line, msg);

		vfprintf(log_file, buffer, args);
		fclose(log_file);
		free(buffer);
		va_end(args);
	}
#else
	va_list args;
	va_start (args, line);
	char *msg = va_arg(args,char*);
	char *log_buffer = (char *)calloc(LOG_BUFFER_SIZE, sizeof(char));
	char *time_buffer = (char *)calloc(TIME_BUFFER_SIZE, sizeof(char));
	time_t now = time(0);
	struct tm tstruct;
	tstruct = *localtime(&now);
	strftime(time_buffer, TIME_BUFFER_SIZE, "%d-%m-%Y %X", &tstruct);
	// Log file format
	sprintf(log_buffer, "[%s] %-5s %-25s%-25s%-5i %s\n", time_buffer, severity,
			file, func, line, msg);
	vprintf(log_buffer, args);
	free(log_buffer);
	free(time_buffer);
	va_end(args);
#endif
}
