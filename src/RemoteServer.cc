/*
 * RemoteServer.cpp
 *
 *  Created on: Sep 22, 2015
 *      Author: messier
 */

#include "RemoteServer.h"

namespace iHearTech {

RemoteServer::RemoteServer() {
	// TODO Auto-generated constructor stub

}

RemoteServer::~RemoteServer() {
	// TODO Auto-generated destructor stub
}

size_t write_data(void *ptr, size_t size, size_t nmemb, struct url_data *data) {
	size_t index = data->size;
	size_t n = (size * nmemb);
	char* tmp;

	data->size += (size * nmemb);

#ifdef DEBUG
	DEBUG_INFO("data at %p size=%ld nmemb=%ld", ptr, size, nmemb);
#endif
	tmp = (char *)realloc(data->data, data->size + 1); /* +1 for '\0' */

	if(tmp) {
		data->data = tmp;
	} else {
		if(data->data) {
			free(data->data);
		}
		DEBUG_INFO("Failed to allocate memory");
		return 0;
	}

	memcpy((data->data + index), ptr, n);
	data->data[data->size] = '\0';

	return size * nmemb;
}

int RemoteServer::sendData(void *ptr, size_t size, size_t nmemb, struct url_data *data){
	CURL *curl;
	CURLcode res;
	int check = 1;
	curl_global_init(CURL_GLOBAL_ALL);
	curl = curl_easy_init();
	if(curl) {
		curl_easy_setopt(curl, CURLOPT_URL, SEND_URL);
		curl_easy_setopt(curl, CURLOPT_POSTFIELDS, data);
		res = curl_easy_perform(curl);
		if(res != CURLE_OK){
			DEBUG_INFO("curl_easy_perform() failed: %s",curl_easy_strerror(res));
			check = 0;
		}
		curl_easy_cleanup(curl);
	}
	curl_global_cleanup();
	return check;
}

std::string RemoteServer::receiveData(){
	CURL *curl;
	struct url_data data;
	data.size = 0;
	data.data = (char*)malloc(4096); /* reasonable size initial buffer */
	if(NULL == data.data) {
		DEBUG_INFO("Failed to allocate memory");
		abort();
	}
	data.data[0] = '\0';
	CURLcode res;
	curl = curl_easy_init();
	if (curl) {
		curl_easy_setopt(curl, CURLOPT_URL, RECEIVE_URL);
		curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_data);
		curl_easy_setopt(curl, CURLOPT_WRITEDATA, &data);
		res = curl_easy_perform(curl);
		if(res != CURLE_OK) {
			DEBUG_INFO("Can't access to server");
			//DEBUG_INFO("curl_easy_perform() failed: %s\n",
			//            curl_easy_strerror(res));
		}
		curl_easy_cleanup(curl);
	}
	return std::string(data.data);
}

} /* namespace iHearTech */
