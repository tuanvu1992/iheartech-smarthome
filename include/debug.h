/*
 * debug.h
 *
 *  Created on: Jul 3, 2015
 *      Author: messier
 */

#ifndef DEBUG_H_
#define DEBUG_H_

#define DEBUG_INIT()		debug_init()
#define DEBUG_INFO(...) 	debug_log("INFO",__FILE__,__func__,__LINE__,__VA_ARGS__)
#define DEBUG_WARNING(...) 	debug_log("WARN",__FILE__,__func__,__LINE__,__VA_ARGS__)
#define DEBUG_ERROR(...) 	debug_log("ERROR",__FILE__,__func__,__LINE__,__VA_ARGS__)
#define DEBUG_FATAL(...) 	debug_log("FATAL",__FILE__,__func__,__LINE__,__VA_ARGS__)

#define KNRM  "\x1B[0m"
#define KRED  "\x1B[31m"
#define KGRN  "\x1B[32m"
#define KYEL  "\x1B[33m"
#define KBLU  "\x1B[34m"
#define KMAG  "\x1B[35m"
#define KCYN  "\x1B[36m"
#define KWHT  "\x1B[37m"

extern "C" {
extern bool DEBUG_ENABLE;
void debug_init();
void debug_log(const char *severity, const char *file, const char *func, const int line,...);
}
#endif /* DEBUG_H_ */
