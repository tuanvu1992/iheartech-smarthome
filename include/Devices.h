/*
 * Devices.h
 *
 *  Created on: Sep 22, 2015
 *      Author: messier
 */

#ifndef DEVICES_H_
#define DEVICES_H_
#define NUM_OF_DEVICES	27

#include "SimpleGPIO.h"
#include <string>
#include <vector>
#include <string.h>
#include <stdlib.h>
#include "debug.h"

using namespace std;

namespace iHearTech {

enum IOState_t {
	ON = true,
	OFF = false,
};

struct IOMap_t {
	char* name;
	unsigned int ioNum;
	IOState_t state;
};

class Devices {
public:
	Devices();
	virtual ~Devices();
	static void setStateRemote(std::string strPhp);
	static void setStateLocal(std::string strCmd);
	static void init();
private:
	static std::vector<IOMap_t> ioMap;
	static std::vector<IOMap_t> ioMap_cache;
};

} /* namespace iHearTech */

#endif /* DEVICES_H_ */
