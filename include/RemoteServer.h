/*
 * RemoteServer.h
 *
 *  Created on: Sep 22, 2015
 *      Author: messier
 */

#ifndef REMOTESERVER_H_
#define REMOTESERVER_H_

#include <curl/curl.h>
#include <string>
#include <stdlib.h>
#include <string.h>
#include "debug.h"

#define SEND_URL    "http://g4.16mb.com/request.php"
#define RECEIVE_URL "http://g4.16mb.com/index.php"

namespace iHearTech {

struct url_data {
	size_t size;
	char* data;
};

class RemoteServer {
public:
	RemoteServer();
	virtual ~RemoteServer();
	static int sendData(void *ptr, size_t size, size_t nmemb, struct url_data *data);
	static std::string receiveData();

};

} /* namespace iHearTech */

#endif /* REMOTESERVER_H_ */
