/*
 * LocalServer.h
 *
 *  Created on: Oct 5, 2015
 *      Author: messier
 */

#ifndef LOCALSERVER_H_
#define LOCALSERVER_H_

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <string>

#include "debug.h"

namespace iHearTech {

enum LocalMode {
	SERVER,
	CLIENT
};

class LocalServer {
public:
	LocalServer(LocalMode mode, const char *inet_addr);
	virtual ~LocalServer();
	static void send(int connfd, char *msg, int size);
	static int receive(int connfd, char *mgs, int size);
	int acceptConnection();
	static void closeConnection(int connfd);
	int listenfd, servfd;
private:
	struct sockaddr_in serv_addr;
	char sendBuff[1025];
};

} /* namespace iHearTech */

#endif /* LOCALSERVER_H_ */
