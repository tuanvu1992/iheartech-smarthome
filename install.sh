# Simple install script
# Writen by Ho Tuan Vu - July 23, 2015

#!/bin/bash
echo "Building smarthome application..."
cd build/
make clean
make
cd ..
echo "Created binary in build/ folder"
